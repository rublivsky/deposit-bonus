/* eslint-disable func-names */
/* global document */
const inputsPass = document.querySelectorAll('.input-pass');

function showHidePassword() {
  [].forEach.call(inputsPass, (item) => {
    const input = item.querySelector('input[type="password"]');
    const btn = item.querySelector('.input-pass__hidden');

    const showHide = function () {
      if (input.type === 'password') {
        input.type = 'text';
      } else {
        input.type = 'password';
      }

      this.classList.toggle('input-pass__hidden_close');
    };

    btn.addEventListener('click', showHide);

    input.addEventListener('input', (e) => {
      if (e.target.value) {
        btn.classList.add('input-pass__hidden_show');
      } else {
        btn.classList.remove('input-pass__hidden_show');
      }
    });
  });
}

if (inputsPass) {
  showHidePassword();
}