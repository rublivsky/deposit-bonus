(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var phone = $("input.phones-inter");

if (phone.length) {
  phone.each(function () {
    $(this).intlTelInput({
      preferredCountries: ["ua"]
    });
  });
}

},{}],2:[function(require,module,exports){
'use strict';

/* eslint-disable func-names */
/* global document */
var inputsPass = document.querySelectorAll('.input-pass');

function showHidePassword() {
  [].forEach.call(inputsPass, function (item) {
    var input = item.querySelector('input[type="password"]');
    var btn = item.querySelector('.input-pass__hidden');

    var showHide = function showHide() {
      if (input.type === 'password') {
        input.type = 'text';
      } else {
        input.type = 'password';
      }

      this.classList.toggle('input-pass__hidden_close');
    };

    btn.addEventListener('click', showHide);

    input.addEventListener('input', function (e) {
      if (e.target.value) {
        btn.classList.add('input-pass__hidden_show');
      } else {
        btn.classList.remove('input-pass__hidden_show');
      }
    });
  });
}

if (inputsPass) {
  showHidePassword();
}

function isInViewport(node) {
  var rect = node.getBoundingClientRect();
  return (rect.height > 0 || rect.width > 0) && rect.bottom >= 0 && rect.right >= 0 && rect.top <= (window.innerHeight || document.documentElement.clientHeight) && rect.left <= (window.innerWidth || document.documentElement.clientWidth);
}

$(window).scroll(function () {
  var scrolled = $(window).scrollTop();
  $('.parallax').each(function (index, element) {
    var initY = $(this).offset().top;
    var height = $(this).height();
    var endY = initY + $(this).height() / 8;

    // Check if the element is in the viewport.
    var visible = isInViewport(this);
    if (visible) {
      var diff = scrolled - initY;
      var ratio = Math.round(diff / height * 100);
      $(this).css('-webkit-transform', ' translateY(' + parseInt(-(ratio * 1.5)) + 'px)');
    }
  });
});

},{}],3:[function(require,module,exports){
'use strict';

function findVideos() {
	var videos = document.querySelectorAll('.video');

	for (var i = 0; i < videos.length; i++) {
		setupVideo(videos[i]);
	}
}

function setupVideo(video) {
	var link = video.querySelector('.video__link');
	var media = video.querySelector('.video__media');
	var button = video.querySelector('.video__button');
	var id = parseMediaURL(media);

	video.addEventListener('click', function () {
		var iframe = createIframe(id);

		link.remove();
		button.remove();
		video.appendChild(iframe);
	});

	link.removeAttribute('href');
	video.classList.add('video--enabled');
}

function parseMediaURL(media) {
	var regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
	var url = media.src;
	var match = url.match(regexp);

	return match[1];
}

function createIframe(id) {
	var iframe = document.createElement('iframe');

	iframe.setAttribute('allowfullscreen', '');
	iframe.setAttribute('allow', 'autoplay');
	iframe.setAttribute('src', generateURL(id));
	iframe.classList.add('video__media');

	return iframe;
}

function generateURL(id) {
	var query = '?rel=0&showinfo=0&autoplay=1';

	return 'https://www.youtube.com/embed/' + id + query;
}

findVideos();

},{}],4:[function(require,module,exports){
"use strict";

/* eslint-disable */
var mobile = window.matchMedia("(max-width: 992px)").matches;
var someWidth = window.matchMedia("(max-width: 1040px)").matches;
var header = $('.header');

$(window).on('load scroll', function () {
  // Change BG
  if ($(window).scrollTop() > 50) {
    header.addClass('is-minimized');
  } else {
    header.removeClass('is-minimized');
  }
});

},{}],5:[function(require,module,exports){
'use strict';

// Common 
// require('./common/js/aosInit');
require('./common/js/eetPhone');

// Libs

// Components-Blocks
require('./components/blocks/video/video');
require('./components/blocks/input-pass/input-pass');

// Components-Section
require('./components/sections/header/header');

},{"./common/js/eetPhone":1,"./components/blocks/input-pass/input-pass":2,"./components/blocks/video/video":3,"./components/sections/header/header":4}]},{},[5])

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tbW9uL2pzL2VldFBob25lLmpzIiwic3JjL2NvbXBvbmVudHMvYmxvY2tzL2lucHV0LXBhc3MvaW5wdXQtcGFzcy5qcyIsInNyYy9jb21wb25lbnRzL2Jsb2Nrcy92aWRlby92aWRlby5qcyIsInNyYy9jb21wb25lbnRzL3NlY3Rpb25zL2hlYWRlci9oZWFkZXIuanMiLCJzcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBLElBQU0sUUFBUSxFQUFFLG9CQUFGLENBQWQ7O0FBRUEsSUFBSSxNQUFNLE1BQVYsRUFBa0I7QUFDaEIsUUFBTSxJQUFOLENBQVcsWUFBWTtBQUNyQixNQUFFLElBQUYsRUFBUSxZQUFSLENBQXFCO0FBQ25CLDBCQUFvQixDQUFDLElBQUQ7QUFERCxLQUFyQjtBQUdELEdBSkQ7QUFLRDs7Ozs7QUNSRDtBQUNBO0FBQ0EsSUFBTSxhQUFhLFNBQVMsZ0JBQVQsQ0FBMEIsYUFBMUIsQ0FBbkI7O0FBRUEsU0FBUyxnQkFBVCxHQUE0QjtBQUMxQixLQUFHLE9BQUgsQ0FBVyxJQUFYLENBQWdCLFVBQWhCLEVBQTRCLFVBQUMsSUFBRCxFQUFVO0FBQ3BDLFFBQU0sUUFBUSxLQUFLLGFBQUwsQ0FBbUIsd0JBQW5CLENBQWQ7QUFDQSxRQUFNLE1BQU0sS0FBSyxhQUFMLENBQW1CLHFCQUFuQixDQUFaOztBQUVBLFFBQU0sV0FBVyxTQUFYLFFBQVcsR0FBWTtBQUMzQixVQUFJLE1BQU0sSUFBTixLQUFlLFVBQW5CLEVBQStCO0FBQzdCLGNBQU0sSUFBTixHQUFhLE1BQWI7QUFDRCxPQUZELE1BRU87QUFDTCxjQUFNLElBQU4sR0FBYSxVQUFiO0FBQ0Q7O0FBRUQsV0FBSyxTQUFMLENBQWUsTUFBZixDQUFzQiwwQkFBdEI7QUFDRCxLQVJEOztBQVVBLFFBQUksZ0JBQUosQ0FBcUIsT0FBckIsRUFBOEIsUUFBOUI7O0FBRUEsVUFBTSxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxVQUFDLENBQUQsRUFBTztBQUNyQyxVQUFJLEVBQUUsTUFBRixDQUFTLEtBQWIsRUFBb0I7QUFDbEIsWUFBSSxTQUFKLENBQWMsR0FBZCxDQUFrQix5QkFBbEI7QUFDRCxPQUZELE1BRU87QUFDTCxZQUFJLFNBQUosQ0FBYyxNQUFkLENBQXFCLHlCQUFyQjtBQUNEO0FBQ0YsS0FORDtBQU9ELEdBdkJEO0FBd0JEOztBQUVELElBQUksVUFBSixFQUFnQjtBQUNkO0FBQ0Q7O0FBRUQsU0FBUyxZQUFULENBQXNCLElBQXRCLEVBQTRCO0FBQzFCLE1BQUksT0FBTyxLQUFLLHFCQUFMLEVBQVg7QUFDQSxTQUNFLENBQUMsS0FBSyxNQUFMLEdBQWMsQ0FBZCxJQUFtQixLQUFLLEtBQUwsR0FBYSxDQUFqQyxLQUNBLEtBQUssTUFBTCxJQUFlLENBRGYsSUFFQSxLQUFLLEtBQUwsSUFBYyxDQUZkLElBR0EsS0FBSyxHQUFMLEtBQWEsT0FBTyxXQUFQLElBQXNCLFNBQVMsZUFBVCxDQUF5QixZQUE1RCxDQUhBLElBSUEsS0FBSyxJQUFMLEtBQWMsT0FBTyxVQUFQLElBQXFCLFNBQVMsZUFBVCxDQUF5QixXQUE1RCxDQUxGO0FBT0Q7O0FBRUQsRUFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixZQUFXO0FBQzFCLE1BQUksV0FBVyxFQUFFLE1BQUYsRUFBVSxTQUFWLEVBQWY7QUFDQSxJQUFFLFdBQUYsRUFBZSxJQUFmLENBQW9CLFVBQVMsS0FBVCxFQUFnQixPQUFoQixFQUF5QjtBQUMzQyxRQUFJLFFBQVEsRUFBRSxJQUFGLEVBQVEsTUFBUixHQUFpQixHQUE3QjtBQUNBLFFBQUksU0FBUyxFQUFFLElBQUYsRUFBUSxNQUFSLEVBQWI7QUFDQSxRQUFJLE9BQVEsUUFBUSxFQUFFLElBQUYsRUFBUSxNQUFSLEtBQWlCLENBQXJDOztBQUVBO0FBQ0EsUUFBSSxVQUFVLGFBQWEsSUFBYixDQUFkO0FBQ0EsUUFBRyxPQUFILEVBQVk7QUFDVixVQUFJLE9BQU8sV0FBVyxLQUF0QjtBQUNBLFVBQUksUUFBUSxLQUFLLEtBQUwsQ0FBWSxPQUFPLE1BQVIsR0FBa0IsR0FBN0IsQ0FBWjtBQUNBLFFBQUUsSUFBRixFQUFRLEdBQVIsQ0FBWSxtQkFBWixFQUFnQyxpQkFBaUIsU0FBUyxFQUFFLFFBQVEsR0FBVixDQUFULENBQWpCLEdBQTRDLEtBQTVFO0FBQ0Q7QUFDRixHQVpEO0FBYUQsQ0FmRDs7Ozs7QUM5Q0EsU0FBUyxVQUFULEdBQXNCO0FBQ3JCLEtBQUksU0FBUyxTQUFTLGdCQUFULENBQTBCLFFBQTFCLENBQWI7O0FBRUEsTUFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLE9BQU8sTUFBM0IsRUFBbUMsR0FBbkMsRUFBd0M7QUFDdkMsYUFBVyxPQUFPLENBQVAsQ0FBWDtBQUNBO0FBQ0Q7O0FBRUQsU0FBUyxVQUFULENBQW9CLEtBQXBCLEVBQTJCO0FBQzFCLEtBQUksT0FBTyxNQUFNLGFBQU4sQ0FBb0IsY0FBcEIsQ0FBWDtBQUNBLEtBQUksUUFBUSxNQUFNLGFBQU4sQ0FBb0IsZUFBcEIsQ0FBWjtBQUNBLEtBQUksU0FBUyxNQUFNLGFBQU4sQ0FBb0IsZ0JBQXBCLENBQWI7QUFDQSxLQUFJLEtBQUssY0FBYyxLQUFkLENBQVQ7O0FBRUEsT0FBTSxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxZQUFNO0FBQ3JDLE1BQUksU0FBUyxhQUFhLEVBQWIsQ0FBYjs7QUFFQSxPQUFLLE1BQUw7QUFDQSxTQUFPLE1BQVA7QUFDQSxRQUFNLFdBQU4sQ0FBa0IsTUFBbEI7QUFDQSxFQU5EOztBQVFBLE1BQUssZUFBTCxDQUFxQixNQUFyQjtBQUNBLE9BQU0sU0FBTixDQUFnQixHQUFoQixDQUFvQixnQkFBcEI7QUFDQTs7QUFFRCxTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsRUFBOEI7QUFDN0IsS0FBSSxTQUFTLG9FQUFiO0FBQ0EsS0FBSSxNQUFNLE1BQU0sR0FBaEI7QUFDQSxLQUFJLFFBQVEsSUFBSSxLQUFKLENBQVUsTUFBVixDQUFaOztBQUVBLFFBQU8sTUFBTSxDQUFOLENBQVA7QUFDQTs7QUFFRCxTQUFTLFlBQVQsQ0FBc0IsRUFBdEIsRUFBMEI7QUFDekIsS0FBSSxTQUFTLFNBQVMsYUFBVCxDQUF1QixRQUF2QixDQUFiOztBQUVBLFFBQU8sWUFBUCxDQUFvQixpQkFBcEIsRUFBdUMsRUFBdkM7QUFDQSxRQUFPLFlBQVAsQ0FBb0IsT0FBcEIsRUFBNkIsVUFBN0I7QUFDQSxRQUFPLFlBQVAsQ0FBb0IsS0FBcEIsRUFBMkIsWUFBWSxFQUFaLENBQTNCO0FBQ0EsUUFBTyxTQUFQLENBQWlCLEdBQWpCLENBQXFCLGNBQXJCOztBQUVBLFFBQU8sTUFBUDtBQUNBOztBQUVELFNBQVMsV0FBVCxDQUFxQixFQUFyQixFQUF5QjtBQUN4QixLQUFJLFFBQVEsOEJBQVo7O0FBRUEsUUFBTyxtQ0FBbUMsRUFBbkMsR0FBd0MsS0FBL0M7QUFDQTs7QUFFRDs7Ozs7QUNuREE7QUFDQSxJQUFNLFNBQVMsT0FBTyxVQUFQLENBQWtCLG9CQUFsQixFQUF3QyxPQUF2RDtBQUNBLElBQU0sWUFBWSxPQUFPLFVBQVAsQ0FBa0IscUJBQWxCLEVBQXlDLE9BQTNEO0FBQ0EsSUFBTSxTQUFTLEVBQUUsU0FBRixDQUFmOztBQUdBLEVBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxhQUFiLEVBQTRCLFlBQVk7QUFDdEM7QUFDQSxNQUFJLEVBQUUsTUFBRixFQUFVLFNBQVYsS0FBd0IsRUFBNUIsRUFBZ0M7QUFDOUIsV0FBTyxRQUFQLENBQWdCLGNBQWhCO0FBQ0QsR0FGRCxNQUVPO0FBQ0wsV0FBTyxXQUFQLENBQW1CLGNBQW5CO0FBQ0Q7QUFDRixDQVBEOzs7OztBQ05BO0FBQ0E7QUFDQSxRQUFRLHNCQUFSOztBQUVBOztBQUVBO0FBQ0EsUUFBUSxpQ0FBUjtBQUNBLFFBQVEsMkNBQVI7O0FBRUE7QUFDQSxRQUFRLHFDQUFSIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsImNvbnN0IHBob25lID0gJChcImlucHV0LnBob25lcy1pbnRlclwiKTtcclxuXHJcbmlmIChwaG9uZS5sZW5ndGgpIHtcclxuICBwaG9uZS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykuaW50bFRlbElucHV0KHtcclxuICAgICAgcHJlZmVycmVkQ291bnRyaWVzOiBbXCJ1YVwiXSxcclxuICAgIH0pO1xyXG4gIH0pOyAgXHJcbn0iLCIvKiBlc2xpbnQtZGlzYWJsZSBmdW5jLW5hbWVzICovXG4vKiBnbG9iYWwgZG9jdW1lbnQgKi9cbmNvbnN0IGlucHV0c1Bhc3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuaW5wdXQtcGFzcycpO1xuXG5mdW5jdGlvbiBzaG93SGlkZVBhc3N3b3JkKCkge1xuICBbXS5mb3JFYWNoLmNhbGwoaW5wdXRzUGFzcywgKGl0ZW0pID0+IHtcbiAgICBjb25zdCBpbnB1dCA9IGl0ZW0ucXVlcnlTZWxlY3RvcignaW5wdXRbdHlwZT1cInBhc3N3b3JkXCJdJyk7XG4gICAgY29uc3QgYnRuID0gaXRlbS5xdWVyeVNlbGVjdG9yKCcuaW5wdXQtcGFzc19faGlkZGVuJyk7XG5cbiAgICBjb25zdCBzaG93SGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChpbnB1dC50eXBlID09PSAncGFzc3dvcmQnKSB7XG4gICAgICAgIGlucHV0LnR5cGUgPSAndGV4dCc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbnB1dC50eXBlID0gJ3Bhc3N3b3JkJztcbiAgICAgIH1cblxuICAgICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKCdpbnB1dC1wYXNzX19oaWRkZW5fY2xvc2UnKTtcbiAgICB9O1xuXG4gICAgYnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgc2hvd0hpZGUpO1xuXG4gICAgaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCAoZSkgPT4ge1xuICAgICAgaWYgKGUudGFyZ2V0LnZhbHVlKSB7XG4gICAgICAgIGJ0bi5jbGFzc0xpc3QuYWRkKCdpbnB1dC1wYXNzX19oaWRkZW5fc2hvdycpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYnRuLmNsYXNzTGlzdC5yZW1vdmUoJ2lucHV0LXBhc3NfX2hpZGRlbl9zaG93Jyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufVxuXG5pZiAoaW5wdXRzUGFzcykge1xuICBzaG93SGlkZVBhc3N3b3JkKCk7XG59XG5cbmZ1bmN0aW9uIGlzSW5WaWV3cG9ydChub2RlKSB7XG4gIHZhciByZWN0ID0gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICByZXR1cm4gKFxuICAgIChyZWN0LmhlaWdodCA+IDAgfHwgcmVjdC53aWR0aCA+IDApICYmXG4gICAgcmVjdC5ib3R0b20gPj0gMCAmJlxuICAgIHJlY3QucmlnaHQgPj0gMCAmJlxuICAgIHJlY3QudG9wIDw9ICh3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCkgJiZcbiAgICByZWN0LmxlZnQgPD0gKHdpbmRvdy5pbm5lcldpZHRoIHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aClcbiAgKVxufVxuXG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uKCkge1xuICB2YXIgc2Nyb2xsZWQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKClcbiAgJCgnLnBhcmFsbGF4JykuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudCkge1xuICAgIHZhciBpbml0WSA9ICQodGhpcykub2Zmc2V0KCkudG9wXG4gICAgdmFyIGhlaWdodCA9ICQodGhpcykuaGVpZ2h0KClcbiAgICB2YXIgZW5kWSAgPSBpbml0WSArICQodGhpcykuaGVpZ2h0KCkvOFxuXG4gICAgLy8gQ2hlY2sgaWYgdGhlIGVsZW1lbnQgaXMgaW4gdGhlIHZpZXdwb3J0LlxuICAgIHZhciB2aXNpYmxlID0gaXNJblZpZXdwb3J0KHRoaXMpXG4gICAgaWYodmlzaWJsZSkge1xuICAgICAgdmFyIGRpZmYgPSBzY3JvbGxlZCAtIGluaXRZXG4gICAgICB2YXIgcmF0aW8gPSBNYXRoLnJvdW5kKChkaWZmIC8gaGVpZ2h0KSAqIDEwMClcbiAgICAgICQodGhpcykuY3NzKCctd2Via2l0LXRyYW5zZm9ybScsJyB0cmFuc2xhdGVZKCcgKyBwYXJzZUludCgtKHJhdGlvICogMS41KSkgKyAncHgpJylcbiAgICB9XG4gIH0pXG59KSIsImZ1bmN0aW9uIGZpbmRWaWRlb3MoKSB7XHJcblx0bGV0IHZpZGVvcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy52aWRlbycpO1xyXG5cclxuXHRmb3IgKGxldCBpID0gMDsgaSA8IHZpZGVvcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0c2V0dXBWaWRlbyh2aWRlb3NbaV0pO1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gc2V0dXBWaWRlbyh2aWRlbykge1xyXG5cdGxldCBsaW5rID0gdmlkZW8ucXVlcnlTZWxlY3RvcignLnZpZGVvX19saW5rJyk7XHJcblx0bGV0IG1lZGlhID0gdmlkZW8ucXVlcnlTZWxlY3RvcignLnZpZGVvX19tZWRpYScpO1xyXG5cdGxldCBidXR0b24gPSB2aWRlby5xdWVyeVNlbGVjdG9yKCcudmlkZW9fX2J1dHRvbicpO1xyXG5cdGxldCBpZCA9IHBhcnNlTWVkaWFVUkwobWVkaWEpO1xyXG5cclxuXHR2aWRlby5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuXHRcdGxldCBpZnJhbWUgPSBjcmVhdGVJZnJhbWUoaWQpO1xyXG5cclxuXHRcdGxpbmsucmVtb3ZlKCk7XHJcblx0XHRidXR0b24ucmVtb3ZlKCk7XHJcblx0XHR2aWRlby5hcHBlbmRDaGlsZChpZnJhbWUpO1xyXG5cdH0pO1xyXG5cclxuXHRsaW5rLnJlbW92ZUF0dHJpYnV0ZSgnaHJlZicpO1xyXG5cdHZpZGVvLmNsYXNzTGlzdC5hZGQoJ3ZpZGVvLS1lbmFibGVkJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHBhcnNlTWVkaWFVUkwobWVkaWEpIHtcclxuXHRsZXQgcmVnZXhwID0gL2h0dHBzOlxcL1xcL2lcXC55dGltZ1xcLmNvbVxcL3ZpXFwvKFthLXpBLVowLTlfLV0rKVxcL21heHJlc2RlZmF1bHRcXC5qcGcvaTtcclxuXHRsZXQgdXJsID0gbWVkaWEuc3JjO1xyXG5cdGxldCBtYXRjaCA9IHVybC5tYXRjaChyZWdleHApO1xyXG5cclxuXHRyZXR1cm4gbWF0Y2hbMV07XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNyZWF0ZUlmcmFtZShpZCkge1xyXG5cdGxldCBpZnJhbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpZnJhbWUnKTtcclxuXHJcblx0aWZyYW1lLnNldEF0dHJpYnV0ZSgnYWxsb3dmdWxsc2NyZWVuJywgJycpO1xyXG5cdGlmcmFtZS5zZXRBdHRyaWJ1dGUoJ2FsbG93JywgJ2F1dG9wbGF5Jyk7XHJcblx0aWZyYW1lLnNldEF0dHJpYnV0ZSgnc3JjJywgZ2VuZXJhdGVVUkwoaWQpKTtcclxuXHRpZnJhbWUuY2xhc3NMaXN0LmFkZCgndmlkZW9fX21lZGlhJyk7XHJcblxyXG5cdHJldHVybiBpZnJhbWU7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdlbmVyYXRlVVJMKGlkKSB7XHJcblx0bGV0IHF1ZXJ5ID0gJz9yZWw9MCZzaG93aW5mbz0wJmF1dG9wbGF5PTEnO1xyXG5cclxuXHRyZXR1cm4gJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2VtYmVkLycgKyBpZCArIHF1ZXJ5O1xyXG59XHJcblxyXG5maW5kVmlkZW9zKCk7IiwiLyogZXNsaW50LWRpc2FibGUgKi9cclxuY29uc3QgbW9iaWxlID0gd2luZG93Lm1hdGNoTWVkaWEoXCIobWF4LXdpZHRoOiA5OTJweClcIikubWF0Y2hlcztcclxuY29uc3Qgc29tZVdpZHRoID0gd2luZG93Lm1hdGNoTWVkaWEoXCIobWF4LXdpZHRoOiAxMDQwcHgpXCIpLm1hdGNoZXM7XHJcbmNvbnN0IGhlYWRlciA9ICQoJy5oZWFkZXInKTtcclxuXHJcblxyXG4kKHdpbmRvdykub24oJ2xvYWQgc2Nyb2xsJywgZnVuY3Rpb24gKCkge1xyXG4gIC8vIENoYW5nZSBCR1xyXG4gIGlmICgkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiA1MCkge1xyXG4gICAgaGVhZGVyLmFkZENsYXNzKCdpcy1taW5pbWl6ZWQnKTtcclxuICB9IGVsc2Uge1xyXG4gICAgaGVhZGVyLnJlbW92ZUNsYXNzKCdpcy1taW5pbWl6ZWQnKTtcclxuICB9XHJcbn0pO1xyXG5cclxuXHJcblxyXG5cclxuIiwiLy8gQ29tbW9uIFxuLy8gcmVxdWlyZSgnLi9jb21tb24vanMvYW9zSW5pdCcpO1xucmVxdWlyZSgnLi9jb21tb24vanMvZWV0UGhvbmUnKTtcblxuLy8gTGlic1xuXG4vLyBDb21wb25lbnRzLUJsb2Nrc1xucmVxdWlyZSgnLi9jb21wb25lbnRzL2Jsb2Nrcy92aWRlby92aWRlbycpO1xucmVxdWlyZSgnLi9jb21wb25lbnRzL2Jsb2Nrcy9pbnB1dC1wYXNzL2lucHV0LXBhc3MnKTtcblxuLy8gQ29tcG9uZW50cy1TZWN0aW9uXG5yZXF1aXJlKCcuL2NvbXBvbmVudHMvc2VjdGlvbnMvaGVhZGVyL2hlYWRlcicpOyJdLCJwcmVFeGlzdGluZ0NvbW1lbnQiOiIvLyMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsZXlKMlpYSnphVzl1SWpvekxDSnpiM1Z5WTJWeklqcGJJbTV2WkdWZmJXOWtkV3hsY3k5aWNtOTNjMlZ5TFhCaFkyc3ZYM0J5Wld4MVpHVXVhbk1pTENKemNtTXZZMjl0Ylc5dUwycHpMMlZsZEZCb2IyNWxMbXB6SWl3aWMzSmpMMk52YlhCdmJtVnVkSE12WW14dlkydHpMMmx1Y0hWMExYQmhjM012YVc1d2RYUXRjR0Z6Y3k1cWN5SXNJbk55WXk5amIyMXdiMjVsYm5SekwySnNiMk5yY3k5MmFXUmxieTkyYVdSbGJ5NXFjeUlzSW5OeVl5OWpiMjF3YjI1bGJuUnpMM05sWTNScGIyNXpMMmhsWVdSbGNpOW9aV0ZrWlhJdWFuTWlMQ0p6Y21NdmFXNWtaWGd1YW5NaVhTd2libUZ0WlhNaU9sdGRMQ0p0WVhCd2FXNW5jeUk2SWtGQlFVRTdPenRCUTBGQkxFbEJRVTBzVVVGQlVTeEZRVUZGTEc5Q1FVRkdMRU5CUVdRN08wRkJSVUVzU1VGQlNTeE5RVUZOTEUxQlFWWXNSVUZCYTBJN1FVRkRhRUlzVVVGQlRTeEpRVUZPTEVOQlFWY3NXVUZCV1R0QlFVTnlRaXhOUVVGRkxFbEJRVVlzUlVGQlVTeFpRVUZTTEVOQlFYRkNPMEZCUTI1Q0xEQkNRVUZ2UWl4RFFVRkRMRWxCUVVRN1FVRkVSQ3hMUVVGeVFqdEJRVWRFTEVkQlNrUTdRVUZMUkRzN096czdRVU5TUkR0QlFVTkJPMEZCUTBFc1NVRkJUU3hoUVVGaExGTkJRVk1zWjBKQlFWUXNRMEZCTUVJc1lVRkJNVUlzUTBGQmJrSTdPMEZCUlVFc1UwRkJVeXhuUWtGQlZDeEhRVUUwUWp0QlFVTXhRaXhMUVVGSExFOUJRVWdzUTBGQlZ5eEpRVUZZTEVOQlFXZENMRlZCUVdoQ0xFVkJRVFJDTEZWQlFVTXNTVUZCUkN4RlFVRlZPMEZCUTNCRExGRkJRVTBzVVVGQlVTeExRVUZMTEdGQlFVd3NRMEZCYlVJc2QwSkJRVzVDTEVOQlFXUTdRVUZEUVN4UlFVRk5MRTFCUVUwc1MwRkJTeXhoUVVGTUxFTkJRVzFDTEhGQ1FVRnVRaXhEUVVGYU96dEJRVVZCTEZGQlFVMHNWMEZCVnl4VFFVRllMRkZCUVZjc1IwRkJXVHRCUVVNelFpeFZRVUZKTEUxQlFVMHNTVUZCVGl4TFFVRmxMRlZCUVc1Q0xFVkJRU3RDTzBGQlF6ZENMR05CUVUwc1NVRkJUaXhIUVVGaExFMUJRV0k3UVVGRFJDeFBRVVpFTEUxQlJVODdRVUZEVEN4alFVRk5MRWxCUVU0c1IwRkJZU3hWUVVGaU8wRkJRMFE3TzBGQlJVUXNWMEZCU3l4VFFVRk1MRU5CUVdVc1RVRkJaaXhEUVVGelFpd3dRa0ZCZEVJN1FVRkRSQ3hMUVZKRU96dEJRVlZCTEZGQlFVa3NaMEpCUVVvc1EwRkJjVUlzVDBGQmNrSXNSVUZCT0VJc1VVRkJPVUk3TzBGQlJVRXNWVUZCVFN4blFrRkJUaXhEUVVGMVFpeFBRVUYyUWl4RlFVRm5ReXhWUVVGRExFTkJRVVFzUlVGQlR6dEJRVU55UXl4VlFVRkpMRVZCUVVVc1RVRkJSaXhEUVVGVExFdEJRV0lzUlVGQmIwSTdRVUZEYkVJc1dVRkJTU3hUUVVGS0xFTkJRV01zUjBGQlpDeERRVUZyUWl4NVFrRkJiRUk3UVVGRFJDeFBRVVpFTEUxQlJVODdRVUZEVEN4WlFVRkpMRk5CUVVvc1EwRkJZeXhOUVVGa0xFTkJRWEZDTEhsQ1FVRnlRanRCUVVORU8wRkJRMFlzUzBGT1JEdEJRVTlFTEVkQmRrSkVPMEZCZDBKRU96dEJRVVZFTEVsQlFVa3NWVUZCU2l4RlFVRm5RanRCUVVOa08wRkJRMFE3TzBGQlJVUXNVMEZCVXl4WlFVRlVMRU5CUVhOQ0xFbEJRWFJDTEVWQlFUUkNPMEZCUXpGQ0xFMUJRVWtzVDBGQlR5eExRVUZMTEhGQ1FVRk1MRVZCUVZnN1FVRkRRU3hUUVVORkxFTkJRVU1zUzBGQlN5eE5RVUZNTEVkQlFXTXNRMEZCWkN4SlFVRnRRaXhMUVVGTExFdEJRVXdzUjBGQllTeERRVUZxUXl4TFFVTkJMRXRCUVVzc1RVRkJUQ3hKUVVGbExFTkJSR1lzU1VGRlFTeExRVUZMTEV0QlFVd3NTVUZCWXl4RFFVWmtMRWxCUjBFc1MwRkJTeXhIUVVGTUxFdEJRV0VzVDBGQlR5eFhRVUZRTEVsQlFYTkNMRk5CUVZNc1pVRkJWQ3hEUVVGNVFpeFpRVUUxUkN4RFFVaEJMRWxCU1VFc1MwRkJTeXhKUVVGTUxFdEJRV01zVDBGQlR5eFZRVUZRTEVsQlFYRkNMRk5CUVZNc1pVRkJWQ3hEUVVGNVFpeFhRVUUxUkN4RFFVeEdPMEZCVDBRN08wRkJSVVFzUlVGQlJTeE5RVUZHTEVWQlFWVXNUVUZCVml4RFFVRnBRaXhaUVVGWE8wRkJRekZDTEUxQlFVa3NWMEZCVnl4RlFVRkZMRTFCUVVZc1JVRkJWU3hUUVVGV0xFVkJRV1k3UVVGRFFTeEpRVUZGTEZkQlFVWXNSVUZCWlN4SlFVRm1MRU5CUVc5Q0xGVkJRVk1zUzBGQlZDeEZRVUZuUWl4UFFVRm9RaXhGUVVGNVFqdEJRVU16UXl4UlFVRkpMRkZCUVZFc1JVRkJSU3hKUVVGR0xFVkJRVkVzVFVGQlVpeEhRVUZwUWl4SFFVRTNRanRCUVVOQkxGRkJRVWtzVTBGQlV5eEZRVUZGTEVsQlFVWXNSVUZCVVN4TlFVRlNMRVZCUVdJN1FVRkRRU3hSUVVGSkxFOUJRVkVzVVVGQlVTeEZRVUZGTEVsQlFVWXNSVUZCVVN4TlFVRlNMRXRCUVdsQ0xFTkJRWEpET3p0QlFVVkJPMEZCUTBFc1VVRkJTU3hWUVVGVkxHRkJRV0VzU1VGQllpeERRVUZrTzBGQlEwRXNVVUZCUnl4UFFVRklMRVZCUVZrN1FVRkRWaXhWUVVGSkxFOUJRVThzVjBGQlZ5eExRVUYwUWp0QlFVTkJMRlZCUVVrc1VVRkJVU3hMUVVGTExFdEJRVXdzUTBGQldTeFBRVUZQTEUxQlFWSXNSMEZCYTBJc1IwRkJOMElzUTBGQldqdEJRVU5CTEZGQlFVVXNTVUZCUml4RlFVRlJMRWRCUVZJc1EwRkJXU3h0UWtGQldpeEZRVUZuUXl4cFFrRkJhVUlzVTBGQlV5eEZRVUZGTEZGQlFWRXNSMEZCVml4RFFVRlVMRU5CUVdwQ0xFZEJRVFJETEV0QlFUVkZPMEZCUTBRN1FVRkRSaXhIUVZwRU8wRkJZVVFzUTBGbVJEczdPenM3UVVNNVEwRXNVMEZCVXl4VlFVRlVMRWRCUVhOQ08wRkJRM0pDTEV0QlFVa3NVMEZCVXl4VFFVRlRMR2RDUVVGVUxFTkJRVEJDTEZGQlFURkNMRU5CUVdJN08wRkJSVUVzVFVGQlN5eEpRVUZKTEVsQlFVa3NRMEZCWWl4RlFVRm5RaXhKUVVGSkxFOUJRVThzVFVGQk0wSXNSVUZCYlVNc1IwRkJia01zUlVGQmQwTTdRVUZEZGtNc1lVRkJWeXhQUVVGUExFTkJRVkFzUTBGQldEdEJRVU5CTzBGQlEwUTdPMEZCUlVRc1UwRkJVeXhWUVVGVUxFTkJRVzlDTEV0QlFYQkNMRVZCUVRKQ08wRkJRekZDTEV0QlFVa3NUMEZCVHl4TlFVRk5MR0ZCUVU0c1EwRkJiMElzWTBGQmNFSXNRMEZCV0R0QlFVTkJMRXRCUVVrc1VVRkJVU3hOUVVGTkxHRkJRVTRzUTBGQmIwSXNaVUZCY0VJc1EwRkJXanRCUVVOQkxFdEJRVWtzVTBGQlV5eE5RVUZOTEdGQlFVNHNRMEZCYjBJc1owSkJRWEJDTEVOQlFXSTdRVUZEUVN4TFFVRkpMRXRCUVVzc1kwRkJZeXhMUVVGa0xFTkJRVlE3TzBGQlJVRXNUMEZCVFN4blFrRkJUaXhEUVVGMVFpeFBRVUYyUWl4RlFVRm5ReXhaUVVGTk8wRkJRM0pETEUxQlFVa3NVMEZCVXl4aFFVRmhMRVZCUVdJc1EwRkJZanM3UVVGRlFTeFBRVUZMTEUxQlFVdzdRVUZEUVN4VFFVRlBMRTFCUVZBN1FVRkRRU3hSUVVGTkxGZEJRVTRzUTBGQmEwSXNUVUZCYkVJN1FVRkRRU3hGUVU1RU96dEJRVkZCTEUxQlFVc3NaVUZCVEN4RFFVRnhRaXhOUVVGeVFqdEJRVU5CTEU5QlFVMHNVMEZCVGl4RFFVRm5RaXhIUVVGb1FpeERRVUZ2UWl4blFrRkJjRUk3UVVGRFFUczdRVUZGUkN4VFFVRlRMR0ZCUVZRc1EwRkJkVUlzUzBGQmRrSXNSVUZCT0VJN1FVRkROMElzUzBGQlNTeFRRVUZUTEc5RlFVRmlPMEZCUTBFc1MwRkJTU3hOUVVGTkxFMUJRVTBzUjBGQmFFSTdRVUZEUVN4TFFVRkpMRkZCUVZFc1NVRkJTU3hMUVVGS0xFTkJRVlVzVFVGQlZpeERRVUZhT3p0QlFVVkJMRkZCUVU4c1RVRkJUU3hEUVVGT0xFTkJRVkE3UVVGRFFUczdRVUZGUkN4VFFVRlRMRmxCUVZRc1EwRkJjMElzUlVGQmRFSXNSVUZCTUVJN1FVRkRla0lzUzBGQlNTeFRRVUZUTEZOQlFWTXNZVUZCVkN4RFFVRjFRaXhSUVVGMlFpeERRVUZpT3p0QlFVVkJMRkZCUVU4c1dVRkJVQ3hEUVVGdlFpeHBRa0ZCY0VJc1JVRkJkVU1zUlVGQmRrTTdRVUZEUVN4UlFVRlBMRmxCUVZBc1EwRkJiMElzVDBGQmNFSXNSVUZCTmtJc1ZVRkJOMEk3UVVGRFFTeFJRVUZQTEZsQlFWQXNRMEZCYjBJc1MwRkJjRUlzUlVGQk1rSXNXVUZCV1N4RlFVRmFMRU5CUVROQ08wRkJRMEVzVVVGQlR5eFRRVUZRTEVOQlFXbENMRWRCUVdwQ0xFTkJRWEZDTEdOQlFYSkNPenRCUVVWQkxGRkJRVThzVFVGQlVEdEJRVU5CT3p0QlFVVkVMRk5CUVZNc1YwRkJWQ3hEUVVGeFFpeEZRVUZ5UWl4RlFVRjVRanRCUVVONFFpeExRVUZKTEZGQlFWRXNPRUpCUVZvN08wRkJSVUVzVVVGQlR5eHRRMEZCYlVNc1JVRkJia01zUjBGQmQwTXNTMEZCTDBNN1FVRkRRVHM3UVVGRlJEczdPenM3UVVOdVJFRTdRVUZEUVN4SlFVRk5MRk5CUVZNc1QwRkJUeXhWUVVGUUxFTkJRV3RDTEc5Q1FVRnNRaXhGUVVGM1F5eFBRVUYyUkR0QlFVTkJMRWxCUVUwc1dVRkJXU3hQUVVGUExGVkJRVkFzUTBGQmEwSXNjVUpCUVd4Q0xFVkJRWGxETEU5QlFUTkVPMEZCUTBFc1NVRkJUU3hUUVVGVExFVkJRVVVzVTBGQlJpeERRVUZtT3p0QlFVZEJMRVZCUVVVc1RVRkJSaXhGUVVGVkxFVkJRVllzUTBGQllTeGhRVUZpTEVWQlFUUkNMRmxCUVZrN1FVRkRkRU03UVVGRFFTeE5RVUZKTEVWQlFVVXNUVUZCUml4RlFVRlZMRk5CUVZZc1MwRkJkMElzUlVGQk5VSXNSVUZCWjBNN1FVRkRPVUlzVjBGQlR5eFJRVUZRTEVOQlFXZENMR05CUVdoQ08wRkJRMFFzUjBGR1JDeE5RVVZQTzBGQlEwd3NWMEZCVHl4WFFVRlFMRU5CUVcxQ0xHTkJRVzVDTzBGQlEwUTdRVUZEUml4RFFWQkVPenM3T3p0QlEwNUJPMEZCUTBFN1FVRkRRU3hSUVVGUkxITkNRVUZTT3p0QlFVVkJPenRCUVVWQk8wRkJRMEVzVVVGQlVTeHBRMEZCVWp0QlFVTkJMRkZCUVZFc01rTkJRVkk3TzBGQlJVRTdRVUZEUVN4UlFVRlJMSEZEUVVGU0lpd2labWxzWlNJNkltZGxibVZ5WVhSbFpDNXFjeUlzSW5OdmRYSmpaVkp2YjNRaU9pSWlMQ0p6YjNWeVkyVnpRMjl1ZEdWdWRDSTZXeUlvWm5WdVkzUnBiMjRvS1h0bWRXNWpkR2x2YmlCeUtHVXNiaXgwS1h0bWRXNWpkR2x2YmlCdktHa3NaaWw3YVdZb0lXNWJhVjBwZTJsbUtDRmxXMmxkS1h0MllYSWdZejFjSW1aMWJtTjBhVzl1WENJOVBYUjVjR1Z2WmlCeVpYRjFhWEpsSmlaeVpYRjFhWEpsTzJsbUtDRm1KaVpqS1hKbGRIVnliaUJqS0drc0lUQXBPMmxtS0hVcGNtVjBkWEp1SUhVb2FTd2hNQ2s3ZG1GeUlHRTlibVYzSUVWeWNtOXlLRndpUTJGdWJtOTBJR1pwYm1RZ2JXOWtkV3hsSUNkY0lpdHBLMXdpSjF3aUtUdDBhSEp2ZHlCaExtTnZaR1U5WENKTlQwUlZURVZmVGs5VVgwWlBWVTVFWENJc1lYMTJZWElnY0QxdVcybGRQWHRsZUhCdmNuUnpPbnQ5ZlR0bFcybGRXekJkTG1OaGJHd29jQzVsZUhCdmNuUnpMR1oxYm1OMGFXOXVLSElwZTNaaGNpQnVQV1ZiYVYxYk1WMWJjbDA3Y21WMGRYSnVJRzhvYm54OGNpbDlMSEFzY0M1bGVIQnZjblJ6TEhJc1pTeHVMSFFwZlhKbGRIVnliaUJ1VzJsZExtVjRjRzl5ZEhOOVptOXlLSFpoY2lCMVBWd2lablZ1WTNScGIyNWNJajA5ZEhsd1pXOW1JSEpsY1hWcGNtVW1KbkpsY1hWcGNtVXNhVDB3TzJrOGRDNXNaVzVuZEdnN2FTc3JLVzhvZEZ0cFhTazdjbVYwZFhKdUlHOTljbVYwZFhKdUlISjlLU2dwSWl3aVkyOXVjM1FnY0dodmJtVWdQU0FrS0Z3aWFXNXdkWFF1Y0dodmJtVnpMV2x1ZEdWeVhDSXBPMXh5WEc1Y2NseHVhV1lnS0hCb2IyNWxMbXhsYm1kMGFDa2dlMXh5WEc0Z0lIQm9iMjVsTG1WaFkyZ29ablZ1WTNScGIyNGdLQ2tnZTF4eVhHNGdJQ0FnSkNoMGFHbHpLUzVwYm5Sc1ZHVnNTVzV3ZFhRb2UxeHlYRzRnSUNBZ0lDQndjbVZtWlhKeVpXUkRiM1Z1ZEhKcFpYTTZJRnRjSW5WaFhDSmRMRnh5WEc0Z0lDQWdmU2s3WEhKY2JpQWdmU2s3SUNCY2NseHVmU0lzSWk4cUlHVnpiR2x1ZEMxa2FYTmhZbXhsSUdaMWJtTXRibUZ0WlhNZ0tpOWNiaThxSUdkc2IySmhiQ0JrYjJOMWJXVnVkQ0FxTDF4dVkyOXVjM1FnYVc1d2RYUnpVR0Z6Y3lBOUlHUnZZM1Z0Wlc1MExuRjFaWEo1VTJWc1pXTjBiM0pCYkd3b0p5NXBibkIxZEMxd1lYTnpKeWs3WEc1Y2JtWjFibU4wYVc5dUlITm9iM2RJYVdSbFVHRnpjM2R2Y21Rb0tTQjdYRzRnSUZ0ZExtWnZja1ZoWTJndVkyRnNiQ2hwYm5CMWRITlFZWE56TENBb2FYUmxiU2tnUFQ0Z2UxeHVJQ0FnSUdOdmJuTjBJR2x1Y0hWMElEMGdhWFJsYlM1eGRXVnllVk5sYkdWamRHOXlLQ2RwYm5CMWRGdDBlWEJsUFZ3aWNHRnpjM2R2Y21SY0lsMG5LVHRjYmlBZ0lDQmpiMjV6ZENCaWRHNGdQU0JwZEdWdExuRjFaWEo1VTJWc1pXTjBiM0lvSnk1cGJuQjFkQzF3WVhOelgxOW9hV1JrWlc0bktUdGNibHh1SUNBZ0lHTnZibk4wSUhOb2IzZElhV1JsSUQwZ1puVnVZM1JwYjI0Z0tDa2dlMXh1SUNBZ0lDQWdhV1lnS0dsdWNIVjBMblI1Y0dVZ1BUMDlJQ2R3WVhOemQyOXlaQ2NwSUh0Y2JpQWdJQ0FnSUNBZ2FXNXdkWFF1ZEhsd1pTQTlJQ2QwWlhoMEp6dGNiaUFnSUNBZ0lIMGdaV3h6WlNCN1hHNGdJQ0FnSUNBZ0lHbHVjSFYwTG5SNWNHVWdQU0FuY0dGemMzZHZjbVFuTzF4dUlDQWdJQ0FnZlZ4dVhHNGdJQ0FnSUNCMGFHbHpMbU5zWVhOelRHbHpkQzUwYjJkbmJHVW9KMmx1Y0hWMExYQmhjM05mWDJocFpHUmxibDlqYkc5elpTY3BPMXh1SUNBZ0lIMDdYRzVjYmlBZ0lDQmlkRzR1WVdSa1JYWmxiblJNYVhOMFpXNWxjaWduWTJ4cFkyc25MQ0J6YUc5M1NHbGtaU2s3WEc1Y2JpQWdJQ0JwYm5CMWRDNWhaR1JGZG1WdWRFeHBjM1JsYm1WeUtDZHBibkIxZENjc0lDaGxLU0E5UGlCN1hHNGdJQ0FnSUNCcFppQW9aUzUwWVhKblpYUXVkbUZzZFdVcElIdGNiaUFnSUNBZ0lDQWdZblJ1TG1Oc1lYTnpUR2x6ZEM1aFpHUW9KMmx1Y0hWMExYQmhjM05mWDJocFpHUmxibDl6YUc5M0p5azdYRzRnSUNBZ0lDQjlJR1ZzYzJVZ2UxeHVJQ0FnSUNBZ0lDQmlkRzR1WTJ4aGMzTk1hWE4wTG5KbGJXOTJaU2duYVc1d2RYUXRjR0Z6YzE5ZmFHbGtaR1Z1WDNOb2IzY25LVHRjYmlBZ0lDQWdJSDFjYmlBZ0lDQjlLVHRjYmlBZ2ZTazdYRzU5WEc1Y2JtbG1JQ2hwYm5CMWRITlFZWE56S1NCN1hHNGdJSE5vYjNkSWFXUmxVR0Z6YzNkdmNtUW9LVHRjYm4xY2JseHVablZ1WTNScGIyNGdhWE5KYmxacFpYZHdiM0owS0c1dlpHVXBJSHRjYmlBZ2RtRnlJSEpsWTNRZ1BTQnViMlJsTG1kbGRFSnZkVzVrYVc1blEyeHBaVzUwVW1WamRDZ3BYRzRnSUhKbGRIVnliaUFvWEc0Z0lDQWdLSEpsWTNRdWFHVnBaMmgwSUQ0Z01DQjhmQ0J5WldOMExuZHBaSFJvSUQ0Z01Da2dKaVpjYmlBZ0lDQnlaV04wTG1KdmRIUnZiU0ErUFNBd0lDWW1YRzRnSUNBZ2NtVmpkQzV5YVdkb2RDQStQU0F3SUNZbVhHNGdJQ0FnY21WamRDNTBiM0FnUEQwZ0tIZHBibVJ2ZHk1cGJtNWxja2hsYVdkb2RDQjhmQ0JrYjJOMWJXVnVkQzVrYjJOMWJXVnVkRVZzWlcxbGJuUXVZMnhwWlc1MFNHVnBaMmgwS1NBbUpseHVJQ0FnSUhKbFkzUXViR1ZtZENBOFBTQW9kMmx1Wkc5M0xtbHVibVZ5VjJsa2RHZ2dmSHdnWkc5amRXMWxiblF1Wkc5amRXMWxiblJGYkdWdFpXNTBMbU5zYVdWdWRGZHBaSFJvS1Z4dUlDQXBYRzU5WEc1Y2JpUW9kMmx1Wkc5M0tTNXpZM0p2Ykd3b1puVnVZM1JwYjI0b0tTQjdYRzRnSUhaaGNpQnpZM0p2Ykd4bFpDQTlJQ1FvZDJsdVpHOTNLUzV6WTNKdmJHeFViM0FvS1Z4dUlDQWtLQ2N1Y0dGeVlXeHNZWGduS1M1bFlXTm9LR1oxYm1OMGFXOXVLR2x1WkdWNExDQmxiR1Z0Wlc1MEtTQjdYRzRnSUNBZ2RtRnlJR2x1YVhSWklEMGdKQ2gwYUdsektTNXZabVp6WlhRb0tTNTBiM0JjYmlBZ0lDQjJZWElnYUdWcFoyaDBJRDBnSkNoMGFHbHpLUzVvWldsbmFIUW9LVnh1SUNBZ0lIWmhjaUJsYm1SWklDQTlJR2x1YVhSWklDc2dKQ2gwYUdsektTNW9aV2xuYUhRb0tTODRYRzVjYmlBZ0lDQXZMeUJEYUdWamF5QnBaaUIwYUdVZ1pXeGxiV1Z1ZENCcGN5QnBiaUIwYUdVZ2RtbGxkM0J2Y25RdVhHNGdJQ0FnZG1GeUlIWnBjMmxpYkdVZ1BTQnBjMGx1Vm1sbGQzQnZjblFvZEdocGN5bGNiaUFnSUNCcFppaDJhWE5wWW14bEtTQjdYRzRnSUNBZ0lDQjJZWElnWkdsbVppQTlJSE5qY205c2JHVmtJQzBnYVc1cGRGbGNiaUFnSUNBZ0lIWmhjaUJ5WVhScGJ5QTlJRTFoZEdndWNtOTFibVFvS0dScFptWWdMeUJvWldsbmFIUXBJQ29nTVRBd0tWeHVJQ0FnSUNBZ0pDaDBhR2x6S1M1amMzTW9KeTEzWldKcmFYUXRkSEpoYm5ObWIzSnRKeXduSUhSeVlXNXpiR0YwWlZrb0p5QXJJSEJoY25ObFNXNTBLQzBvY21GMGFXOGdLaUF4TGpVcEtTQXJJQ2R3ZUNrbktWeHVJQ0FnSUgxY2JpQWdmU2xjYm4wcElpd2lablZ1WTNScGIyNGdabWx1WkZacFpHVnZjeWdwSUh0Y2NseHVYSFJzWlhRZ2RtbGtaVzl6SUQwZ1pHOWpkVzFsYm5RdWNYVmxjbmxUWld4bFkzUnZja0ZzYkNnbkxuWnBaR1Z2SnlrN1hISmNibHh5WEc1Y2RHWnZjaUFvYkdWMElHa2dQU0F3T3lCcElEd2dkbWxrWlc5ekxteGxibWQwYURzZ2FTc3JLU0I3WEhKY2JseDBYSFJ6WlhSMWNGWnBaR1Z2S0hacFpHVnZjMXRwWFNrN1hISmNibHgwZlZ4eVhHNTlYSEpjYmx4eVhHNW1kVzVqZEdsdmJpQnpaWFIxY0ZacFpHVnZLSFpwWkdWdktTQjdYSEpjYmx4MGJHVjBJR3hwYm1zZ1BTQjJhV1JsYnk1eGRXVnllVk5sYkdWamRHOXlLQ2N1ZG1sa1pXOWZYMnhwYm1zbktUdGNjbHh1WEhSc1pYUWdiV1ZrYVdFZ1BTQjJhV1JsYnk1eGRXVnllVk5sYkdWamRHOXlLQ2N1ZG1sa1pXOWZYMjFsWkdsaEp5azdYSEpjYmx4MGJHVjBJR0oxZEhSdmJpQTlJSFpwWkdWdkxuRjFaWEo1VTJWc1pXTjBiM0lvSnk1MmFXUmxiMTlmWW5WMGRHOXVKeWs3WEhKY2JseDBiR1YwSUdsa0lEMGdjR0Z5YzJWTlpXUnBZVlZTVENodFpXUnBZU2s3WEhKY2JseHlYRzVjZEhacFpHVnZMbUZrWkVWMlpXNTBUR2x6ZEdWdVpYSW9KMk5zYVdOckp5d2dLQ2tnUFQ0Z2UxeHlYRzVjZEZ4MGJHVjBJR2xtY21GdFpTQTlJR055WldGMFpVbG1jbUZ0WlNocFpDazdYSEpjYmx4eVhHNWNkRngwYkdsdWF5NXlaVzF2ZG1Vb0tUdGNjbHh1WEhSY2RHSjFkSFJ2Ymk1eVpXMXZkbVVvS1R0Y2NseHVYSFJjZEhacFpHVnZMbUZ3Y0dWdVpFTm9hV3hrS0dsbWNtRnRaU2s3WEhKY2JseDBmU2s3WEhKY2JseHlYRzVjZEd4cGJtc3VjbVZ0YjNabFFYUjBjbWxpZFhSbEtDZG9jbVZtSnlrN1hISmNibHgwZG1sa1pXOHVZMnhoYzNOTWFYTjBMbUZrWkNnbmRtbGtaVzh0TFdWdVlXSnNaV1FuS1R0Y2NseHVmVnh5WEc1Y2NseHVablZ1WTNScGIyNGdjR0Z5YzJWTlpXUnBZVlZTVENodFpXUnBZU2tnZTF4eVhHNWNkR3hsZENCeVpXZGxlSEFnUFNBdmFIUjBjSE02WEZ3dlhGd3ZhVnhjTG5sMGFXMW5YRnd1WTI5dFhGd3ZkbWxjWEM4b1cyRXRla0V0V2pBdE9WOHRYU3NwWEZ3dmJXRjRjbVZ6WkdWbVlYVnNkRnhjTG1wd1p5OXBPMXh5WEc1Y2RHeGxkQ0IxY213Z1BTQnRaV1JwWVM1emNtTTdYSEpjYmx4MGJHVjBJRzFoZEdOb0lEMGdkWEpzTG0xaGRHTm9LSEpsWjJWNGNDazdYSEpjYmx4eVhHNWNkSEpsZEhWeWJpQnRZWFJqYUZzeFhUdGNjbHh1ZlZ4eVhHNWNjbHh1Wm5WdVkzUnBiMjRnWTNKbFlYUmxTV1p5WVcxbEtHbGtLU0I3WEhKY2JseDBiR1YwSUdsbWNtRnRaU0E5SUdSdlkzVnRaVzUwTG1OeVpXRjBaVVZzWlcxbGJuUW9KMmxtY21GdFpTY3BPMXh5WEc1Y2NseHVYSFJwWm5KaGJXVXVjMlYwUVhSMGNtbGlkWFJsS0NkaGJHeHZkMloxYkd4elkzSmxaVzRuTENBbkp5azdYSEpjYmx4MGFXWnlZVzFsTG5ObGRFRjBkSEpwWW5WMFpTZ25ZV3hzYjNjbkxDQW5ZWFYwYjNCc1lYa25LVHRjY2x4dVhIUnBabkpoYldVdWMyVjBRWFIwY21saWRYUmxLQ2R6Y21NbkxDQm5aVzVsY21GMFpWVlNUQ2hwWkNrcE8xeHlYRzVjZEdsbWNtRnRaUzVqYkdGemMweHBjM1F1WVdSa0tDZDJhV1JsYjE5ZmJXVmthV0VuS1R0Y2NseHVYSEpjYmx4MGNtVjBkWEp1SUdsbWNtRnRaVHRjY2x4dWZWeHlYRzVjY2x4dVpuVnVZM1JwYjI0Z1oyVnVaWEpoZEdWVlVrd29hV1FwSUh0Y2NseHVYSFJzWlhRZ2NYVmxjbmtnUFNBblAzSmxiRDB3Sm5Ob2IzZHBibVp2UFRBbVlYVjBiM0JzWVhrOU1TYzdYSEpjYmx4eVhHNWNkSEpsZEhWeWJpQW5hSFIwY0hNNkx5OTNkM2N1ZVc5MWRIVmlaUzVqYjIwdlpXMWlaV1F2SnlBcklHbGtJQ3NnY1hWbGNuazdYSEpjYm4xY2NseHVYSEpjYm1acGJtUldhV1JsYjNNb0tUc2lMQ0l2S2lCbGMyeHBiblF0WkdsellXSnNaU0FxTDF4eVhHNWpiMjV6ZENCdGIySnBiR1VnUFNCM2FXNWtiM2N1YldGMFkyaE5aV1JwWVNoY0lpaHRZWGd0ZDJsa2RHZzZJRGs1TW5CNEtWd2lLUzV0WVhSamFHVnpPMXh5WEc1amIyNXpkQ0J6YjIxbFYybGtkR2dnUFNCM2FXNWtiM2N1YldGMFkyaE5aV1JwWVNoY0lpaHRZWGd0ZDJsa2RHZzZJREV3TkRCd2VDbGNJaWt1YldGMFkyaGxjenRjY2x4dVkyOXVjM1FnYUdWaFpHVnlJRDBnSkNnbkxtaGxZV1JsY2ljcE8xeHlYRzVjY2x4dVhISmNiaVFvZDJsdVpHOTNLUzV2YmlnbmJHOWhaQ0J6WTNKdmJHd25MQ0JtZFc1amRHbHZiaUFvS1NCN1hISmNiaUFnTHk4Z1EyaGhibWRsSUVKSFhISmNiaUFnYVdZZ0tDUW9kMmx1Wkc5M0tTNXpZM0p2Ykd4VWIzQW9LU0ErSURVd0tTQjdYSEpjYmlBZ0lDQm9aV0ZrWlhJdVlXUmtRMnhoYzNNb0oybHpMVzFwYm1sdGFYcGxaQ2NwTzF4eVhHNGdJSDBnWld4elpTQjdYSEpjYmlBZ0lDQm9aV0ZrWlhJdWNtVnRiM1psUTJ4aGMzTW9KMmx6TFcxcGJtbHRhWHBsWkNjcE8xeHlYRzRnSUgxY2NseHVmU2s3WEhKY2JseHlYRzVjY2x4dVhISmNibHh5WEc0aUxDSXZMeUJEYjIxdGIyNGdYRzR2THlCeVpYRjFhWEpsS0NjdUwyTnZiVzF2Ymk5cWN5OWhiM05KYm1sMEp5azdYRzV5WlhGMWFYSmxLQ2N1TDJOdmJXMXZiaTlxY3k5bFpYUlFhRzl1WlNjcE8xeHVYRzR2THlCTWFXSnpYRzVjYmk4dklFTnZiWEJ2Ym1WdWRITXRRbXh2WTJ0elhHNXlaWEYxYVhKbEtDY3VMMk52YlhCdmJtVnVkSE12WW14dlkydHpMM1pwWkdWdkwzWnBaR1Z2SnlrN1hHNXlaWEYxYVhKbEtDY3VMMk52YlhCdmJtVnVkSE12WW14dlkydHpMMmx1Y0hWMExYQmhjM012YVc1d2RYUXRjR0Z6Y3ljcE8xeHVYRzR2THlCRGIyMXdiMjVsYm5SekxWTmxZM1JwYjI1Y2JuSmxjWFZwY21Vb0p5NHZZMjl0Y0c5dVpXNTBjeTl6WldOMGFXOXVjeTlvWldGa1pYSXZhR1ZoWkdWeUp5azdJbDE5In0=
