// Common 
require('./common/js/eetPhone');

// Libs

// Components-Blocks
require('./components/blocks/video/video');
require('./components/blocks/input-pass/input-pass');

// Components-Section
require('./components/sections/header/header');