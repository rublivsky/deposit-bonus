const phone = $("input.phones-inter");

if (phone.length) {
  phone.each(function () {
    $(this).intlTelInput({
      preferredCountries: ["ua"],
    });
  });  
}